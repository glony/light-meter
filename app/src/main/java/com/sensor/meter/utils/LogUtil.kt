package com.sensor.meter.utils

import android.util.Log
import com.sensor.meter.BuildConfig

/**
 * @author       CT
 * @date         2024/3/4
 * @description 日志工具类
 *
 */
object LogUtil {
  private const val TAG = "Meter"

  private fun print(level:Char,vararg message: Any?){
    if(!BuildConfig.DEBUG){
      return;
    }
    val source = Thread.currentThread().stackTrace[4]
    var log="[%s.%s]".format(source.className,source.methodName)
    message!!.forEach {
      log += "$it"
    }

    when(level){
      'd' -> Log.d(TAG,log)
      'i' -> Log.i(TAG,log)
      'w' -> Log.w(TAG,log)
      'e' -> Log.e(TAG,log)
    }
  }


  fun d(vararg message: Any) {
    print('d',*message)
  }


  fun i(vararg message: Any) {
    print('i',*message)
  }


  fun w(vararg message: Any) {
    print('w',*message)
  }


  fun e(vararg message: Any) {
    print('e',*message)
  }

}