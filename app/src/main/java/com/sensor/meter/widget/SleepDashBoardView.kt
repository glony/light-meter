package com.sensor.meter.widget

import android.animation.ObjectAnimator
import android.content.Context
import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.Path
import android.graphics.PathMeasure
import android.graphics.SweepGradient
import android.util.AttributeSet
import android.view.View
import androidx.annotation.Nullable
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import com.sensor.meter.utils.dp2px


/**
 * @author       CT
 * @date         2024/3/7
 * @description
 *
 */
/**
 * 睡眠刻度仪表盘
 */
class SleepDashBoardView @JvmOverloads constructor(context: Context, @Nullable attrs: AttributeSet? = null, defStyleAttr: Int = 0) : View(context, attrs, defStyleAttr) {
  private var progress:Float = 0f
    get() = field
    set(value){
      field=value
      invalidate()
    }
  private var centerX = 0f
  private var centerY = 0f
  // 绘制圆环的线宽
  private val lineWidth:Float?
  // 绘制圆点线宽
  private val pointWidth:Float?
  // 绘制矩形的线宽
  private val rectangleWidth:Float?
  private var sweepGradient: SweepGradient? = null
  private val paint = Paint(Paint.ANTI_ALIAS_FLAG)
  private val bgPaint = Paint(Paint.ANTI_ALIAS_FLAG)
  private val rectPaint = Paint(Paint.ANTI_ALIAS_FLAG)
  private val pointPaint = Paint(Paint.ANTI_ALIAS_FLAG)
  private var animator: ObjectAnimator? = null
  private val colors = intArrayOf(
    Color.parseColor("#C6BCFF"),
    Color.parseColor("#C6BCFF"),  //浅色
    Color.parseColor("#C6BCFF"),
    Color.parseColor("#C6BBFF"),
    Color.parseColor("#AEA0F6"),
    Color.parseColor("#9A8AEF"),
    Color.parseColor("#9888EF"),
    Color.parseColor("#9888EF")
  )

  init {

    lineWidth = context.dp2px(8f)
    pointWidth =context.dp2px(2f)
    rectangleWidth = context.dp2px(1f)

    paint.setColor(Color.GRAY)
    paint.style = Paint.Style.STROKE
    paint.strokeCap = Paint.Cap.ROUND
    paint.strokeWidth = lineWidth
    bgPaint.setColor(Color.parseColor("#F6F6F6"))
    bgPaint.style = Paint.Style.STROKE
    bgPaint.strokeCap = Paint.Cap.ROUND
    bgPaint.strokeWidth = lineWidth
    pointPaint.setColor(Color.WHITE)
    pointPaint.style = Paint.Style.STROKE
    pointPaint.strokeWidth = pointWidth
    rectPaint.setColor(Color.WHITE)
  }



  fun setSleepProgress(progress: Float) {
    animator = ObjectAnimator.ofFloat(this, "progress", 0f, progress)
    animator!!.duration=2000
    animator!!.interpolator=FastOutSlowInInterpolator()
    animator!!.start()
  }

  override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
    super.onSizeChanged(w, h, oldw, oldh)
    centerX = width / 2f
    centerY = height / 2f
    sweepGradient = SweepGradient(centerX, centerY, colors, null)
    //改变开始渐变的角度(默认从右边开始画圆渐变,旋转90度就从下方开始画圆渐变,旋转之后更好设置颜色渐变值)
    val matrix = Matrix()
    matrix.setRotate(90f, centerX, centerY)
    sweepGradient!!.setLocalMatrix(matrix)
    paint.setShader(sweepGradient)
  }

  override fun onDraw(canvas: Canvas) {
    super.onDraw(canvas)
    // 绘制环形
    drawArc(canvas)
    // 绘制矩形白条
    // draRectangle(canvas);
    // 绘制末尾圆点
    drawPoint(canvas)
  }

  private fun drawArc(canvas: Canvas) {
    val sweepAngle = MAX_ANGLE * progress / 100
    val padding = lineWidth!!.plus(pointWidth!!)
    canvas.drawArc(padding, padding, width - padding, height - padding, START_ANGLE, MAX_ANGLE, false, bgPaint)
    canvas.drawArc(padding, padding, width - padding, height - padding, START_ANGLE, sweepAngle, false, paint)
  }

  private fun draRectangle(canvas: Canvas) {
    val sweepAngle = MAX_ANGLE * progress / 100
    if (sweepAngle > 70) {
      // 旋转到左侧
      canvas.save()
      canvas.rotate(-80f, centerX, centerY)
      canvas.drawRect(centerX - rectangleWidth!!, rectangleWidth!!, centerX + rectangleWidth!!, context.dp2px(20f), rectPaint)
      canvas.restore()
    }
    if (sweepAngle > 150f) {
      //绘制中间矩形
      canvas.drawRect(centerX - rectangleWidth!!, 0f, centerX + rectangleWidth!!, context.dp2px(20f), rectPaint)
    }
    if (sweepAngle > 230f) {
      // 旋转到右侧
      canvas.save()
      canvas.rotate(80f, centerX, centerY)
      canvas.drawRect(centerX - rectangleWidth!!, 0f, centerX + rectangleWidth!!, context.dp2px(20f), rectPaint)
      canvas.restore()
    }
  }

  private fun drawPoint(canvas: Canvas) {
    val path = Path()
    val sweepAngle = MAX_ANGLE * progress / 100
    val padding = lineWidth!! + pointWidth!!
    path.addArc(padding, padding, width - padding, height - padding, START_ANGLE, sweepAngle)
    val measure = PathMeasure(path, false)
    val pos = FloatArray(2)
    measure.getPosTan(measure.length - 1, pos, null)
    pointPaint.color=Color.parseColor("#7F43FB")
    pointPaint.style = Paint.Style.FILL //仅填充内部

    canvas.drawCircle(pos[0], pos[1], (lineWidth!! - pointWidth!!).toFloat(), pointPaint)
    pointPaint.color=Color.WHITE
    pointPaint.style = Paint.Style.STROKE //仅描边
    canvas.drawCircle(pos[0], pos[1], (lineWidth!! - pointWidth!!).toFloat(), pointPaint)
  }


  companion object {
    private const val START_ANGLE = 135f
    private const val MAX_ANGLE = 270f
  }
}