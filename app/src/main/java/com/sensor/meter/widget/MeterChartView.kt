package com.sensor.meter.widget

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import kotlin.math.cos
import kotlin.math.sin

/**
 * @author       CT
 * @date         2024/3/7
 * @description 仪表View
 *
 */
class MeterChartView @JvmOverloads constructor(context: Context?, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : View(context, attrs, defStyleAttr) {
  private var widthMode = 0
  private var heightMode = 0
  private var widthSize = 0
  private var heightSize = 0
  private var linePaint: Paint? = null
  private var fullPaint: Paint? = null
  private var textPaint: Paint? = null
  private var cX = 0f
  private var cY = 0f
  private var RADIUS = 0f
  private var dataMax = 240f //最大值
  private var sectionNumber = 6f //区间个数
  private var scaleNumber = 24f //刻度个数
  private var data = 0f //当前数据值
  private var unit = ""
  private var refreshData = 0f


  override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
    super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    widthMode = MeasureSpec.getMode(widthMeasureSpec)
    heightMode = MeasureSpec.getMode(heightMeasureSpec)
    widthSize = MeasureSpec.getSize(widthMeasureSpec)
    heightSize = MeasureSpec.getSize(heightMeasureSpec)
    cX = widthSize / 2f
    cY = heightSize / 2f
    RADIUS = if (cY < cX) cY - 20 else cX - 20

    linePaint = Paint()
    linePaint!!.isAntiAlias = true //消除锯齿
    linePaint!!.color=Color.WHITE
    linePaint!!.strokeWidth = 4f
    linePaint!!.style = Paint.Style.STROKE //决定样式，填充还是画线
    fullPaint = Paint()
    fullPaint!!.isAntiAlias = true //消除锯齿
    fullPaint!!.strokeWidth = 4f

    textPaint = Paint()
    textPaint!!.isAntiAlias = true //消除锯齿
    textPaint!!.textSize = 30f
    textPaint!!.color=Color.WHITE
    textPaint!!.textAlign = Paint.Align.CENTER
  }

  @SuppressLint("DrawAllocation")
  override fun onDraw(canvas: Canvas) {
    super.onDraw(canvas)

    //刷屏幕扇形特效
    val mRectRefresh = RectF(cX - RADIUS, cY - RADIUS, cX + RADIUS, cY + RADIUS)
    fullPaint!!.color=Color.RED
    val refreshStep = 240 / dataMax
    canvas.drawArc(mRectRefresh, 150f, refreshData * refreshStep, true, fullPaint!!)

    //绘制背景黑色扇形
    fullPaint!!.color=Color.BLACK
    canvas.drawCircle(cX, cY, RADIUS - 20, fullPaint!!)

    //绘制扇形
    linePaint!!.color=Color.WHITE
    val mRectF = RectF(cX - RADIUS, cY - RADIUS, cX + RADIUS, cY + RADIUS)
    canvas.drawArc(mRectF, 150f, 240f, true, linePaint!!)

    //绘制遮罩扇形
    fullPaint!!.color=Color.BLACK
    canvas.drawCircle(cX, cY, RADIUS - 50, fullPaint!!)

    //绘制细线圆
    linePaint!!.strokeWidth = 2f
    canvas.drawCircle(cX, cY, RADIUS - 20, linePaint!!)
    val sectionAngle = 240 / sectionNumber //每个大区间的角度值
    val sectionStep = dataMax / sectionNumber


    run {
      var k = 0
      while (k < sectionNumber + 1) {
        val keDuStartX = cX + RADIUS * cos((150 + sectionAngle * k) / 180 * Math.PI).toFloat()
        val keDuStartY = cY + RADIUS * sin((150 + sectionAngle * k) / 180 * Math.PI).toFloat()
        val keDuEndX = cX + (RADIUS - 50) * cos((150 + sectionAngle * k) / 180 * Math.PI).toFloat()
        val keDuEndY = cY + (RADIUS - 50) * sin((150 + sectionAngle * k) / 180 * Math.PI).toFloat()
        //绘制区间刻度线
        linePaint!!.strokeWidth = 4f
        canvas.drawLine(keDuStartX, keDuStartY, keDuEndX, keDuEndY, linePaint!!)
        val textValue = (k * sectionStep).toInt()
        //绘制刻度值
        if (keDuEndX < cX) {
          //左侧刻度
          textPaint!!.color=Color.parseColor("#FF9800")
          textPaint!!.textSize = 30f
          canvas.drawText(textValue.toString() + "", keDuEndX + 30, keDuEndY + 10, textPaint!!)
        } else if (keDuEndX == cX) {
          //中间刻度
          textPaint!!.color=Color.parseColor("#FF9800")
          textPaint!!.textSize = 30f
          canvas.drawText(textValue.toString() + "", keDuEndX, keDuEndY + 30, textPaint!!)
        } else {
          //右侧刻度
          textPaint!!.color=Color.parseColor("#FF9800")
          textPaint!!.textSize = 30f
          canvas.drawText(textValue.toString() + "", keDuEndX - 30, keDuEndY + 10, textPaint!!)
        }
        k++
      }
    }
    val scaleAngle = 240 / scaleNumber //每个刻度的角度值
    var k = 0
    while (k < scaleNumber) {
      val keDuStartX = cX + RADIUS * cos((150 + scaleAngle * k) / 180 * Math.PI).toFloat()
      val keDuStartY = cY + RADIUS * sin((150 + scaleAngle * k) / 180 * Math.PI).toFloat()
      val keDuEndX = cX + (RADIUS - 30) * cos((150 + scaleAngle * k) / 180 * Math.PI).toFloat()
      val keDuEndY = cY + (RADIUS - 30) * sin((150 + scaleAngle * k) / 180 * Math.PI).toFloat()

      //绘制细刻度线
      linePaint!!.strokeWidth = 2f
      canvas.drawLine(keDuStartX, keDuStartY, keDuEndX, keDuEndY, linePaint!!)
      k++
    }

    //绘制指针
    val dataStep = 240 / dataMax
    val pointerStartX = cX
    val pointerStartY = cY
    val pointerEndX = cX + (RADIUS - 40) * cos((150 + data * dataStep) / 180 * Math.PI).toFloat()
    val pointerEndY = cY + (RADIUS - 40) * sin((150 + data * dataStep) / 180 * Math.PI).toFloat()
    linePaint!!.strokeWidth = 6f
    linePaint!!.color=Color.RED
    canvas.drawLine(pointerStartX, pointerStartY, pointerEndX, pointerEndY, linePaint!!)
    fullPaint!!.color=Color.RED
    canvas.drawCircle(cX, cY, 20f, fullPaint!!)
    //绘制数据值
    val nowData = data.toInt()
    textPaint!!.color=Color.WHITE
    textPaint!!.textSize = 40f
    canvas.drawText(nowData.toString() + unit, cX, cY + RADIUS / 2f, textPaint!!)
  }

  /**
   * 初始化轴数据
   * @param dataMax
   * @param sectionNumber
   * @param scaleNumber
   */
  fun initConstant(dataMax: Int, sectionNumber: Int, scaleNumber: Int) {
    this.dataMax = dataMax.toFloat()
    this.sectionNumber = sectionNumber.toFloat()
    this.scaleNumber = scaleNumber.toFloat()
    postInvalidate()
  }

  /**
   * 设置数据
   * @param dataValue 数据
   * @param unit 单位
   */
  fun setData(dataValue: Int, unit: String) {
    data = dataValue.toFloat()
    this.unit = unit
    postInvalidate()
  }

  /**
   * 刷屏幕特效
   * @param data
   */
  fun refreshUi(data: Int) {
    refreshData = data.toFloat()
    postInvalidate()
  }
}


