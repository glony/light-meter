package com.sensor.meter.activity

import android.content.Intent
import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageButton
import click
import com.sensor.meter.R

/**
 * @author       CT
 * @date         2024/3/5
 * @description  主页
 *
 */
class MainActivity : AppCompatActivity(){

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    setTitle(R.string.app_name)

    // 光测仪
    val btnLightMeter= findViewById<AppCompatImageButton>(R.id.btn_light_meter)
    btnLightMeter.click {
      val intent= Intent(this,LightMeter::class.java)
      startActivity(intent)
    }

    // 分贝仪
    findViewById<AppCompatImageButton>(R.id.btn_sound_meter).click {
        startActivity(Intent(this,SoundMeter::class.java))
    }
  }


}