package com.sensor.meter.activity

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import com.sensor.meter.R
import com.sensor.meter.fragment.DialogFra
import com.sensor.meter.utils.LogUtil
import com.sensor.meter.widget.DashboardView

/**
 * 光测仪
 * @property mSensorManager SensorManager
 * @property dashboradView DashboardView?
 * @property tvMinValue AppCompatTextView?
 * @property tvAverageValue AppCompatTextView?
 * @property tvMaxValue AppCompatTextView?
 * @property minValue Int
 * @property maxValue Int
 * @property dataLst MutableList<Int>
 */
class LightMeter : AppCompatActivity(), SensorEventListener {
  private lateinit var mSensorManager: SensorManager
  private var dashboradView:DashboardView?=null
  private var tvMinValue:AppCompatTextView?=null
  private var tvAverageValue:AppCompatTextView?=null
  private var tvMaxValue:AppCompatTextView?=null

  private var minValue:Int=0
  private var maxValue:Int=0

  private val dataLst= mutableListOf<Int>(0)

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_light_meter)
    val actionBar = supportActionBar
    actionBar!!.setHomeButtonEnabled(true)
    actionBar!!.setDisplayHomeAsUpEnabled(true)
    setTitle(R.string.title_light_meter)
    //
    mSensorManager=getSystemService(Context.SENSOR_SERVICE) as SensorManager

    dashboradView=findViewById<DashboardView>(R.id.dashboard)
    tvMinValue=findViewById(R.id.tv_min_value)
    tvAverageValue=findViewById(R.id.tv_average_value)
    tvMaxValue=findViewById(R.id.tv_max_value)
    updateValue()
  }

  override fun onResume() {
    super.onResume()
    mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT),
      SensorManager.SENSOR_DELAY_NORMAL)
  }

  override fun onPause() {
    super.onPause()
    mSensorManager.unregisterListener(this)
  }


  override fun onCreateOptionsMenu(menu: Menu?): Boolean {
    val inflater: MenuInflater = menuInflater
    inflater.inflate(R.menu.options_menu, menu)
    return super.onCreateOptionsMenu(menu)
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    when(item.itemId){
      //onBackPressed()
      android.R.id.home -> onBackPressedDispatcher!!.onBackPressed()
      R.id.action_desc -> showDescFragment()
    }
    return super.onOptionsItemSelected(item)
  }

  private fun showDescFragment(){
    val dialog=DialogFra(R.layout.dialog_light_meter_desc)
    dialog.showDialogFragment(supportFragmentManager)
  }

  override fun onSensorChanged(event: SensorEvent?) {
    if(event!!.sensor.type == Sensor.TYPE_LIGHT){
      val luxValue:Int=event!!.values[0].toInt()
      LogUtil.d("onSensorChanged: luxValue= ",luxValue)
      dashboradView!!.creditValue=luxValue;
      dataLst.add(luxValue)
      if(minValue<=0){
        minValue=luxValue
      }
      if (maxValue<=0){
        maxValue=luxValue
      }
      if(luxValue< minValue ){
        minValue=luxValue
      }else if(luxValue > maxValue){
        maxValue=luxValue
      }
      updateValue()
    }
  }

  fun updateValue(){
    tvMinValue!!.text= minValue.toString()
    tvAverageValue!!.text=dataLst.average()!!.toInt().toString()
    tvMaxValue!!.text=maxValue.toString()
  }

  override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
    LogUtil.d("onAccuracyChanged: accuracy: ",accuracy)
  }
}